package com.company;


import org.jgraph.graph.DefaultEdge;
import org.jgrapht.VertexFactory;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.generate.GnmRandomGraphGenerator;
import org.jgrapht.graph.Multigraph;

import java.util.HashMap;
import java.util.Random;
import java.util.Set;

public class KargerAlgo {
    Multigraph<String, DefaultEdge> stringGraph;
    HashMap<Integer, Integer> results;
    public KargerAlgo() {

        results= new HashMap<>();
        for(int i=0;i<1000;i++) {
            stringGraph = createStringGraph();
            ConnectivityInspector inspector = new ConnectivityInspector(stringGraph);
            while (!inspector.isGraphConnected()) {
                stringGraph = createStringGraph();
            }
            while (stringGraph.vertexSet().size() != 2) {
                contractEdges();
            }
            if (results.putIfAbsent(stringGraph.edgeSet().size(), 1) != null) {
                results.put(stringGraph.edgeSet().size(), results.get(stringGraph.edgeSet().size()) + 1);
            }
        }
        for (int key : results.keySet()) {
            System.out.println("Coupe : "+key+"  frequency: "+results.get(key)+"/1000");
        }
    }

    /**
     * Create a toy graph based on String objects.
     *
     * @return a graph based on String objects.
     */
    private static Multigraph<String, DefaultEdge> createStringGraph()
    {

        Multigraph<String, DefaultEdge> g = new Multigraph<>(DefaultEdge.class);
        long seed = System.currentTimeMillis() / 1000;
        GnmRandomGraphGenerator<String, DefaultEdge> generator= new GnmRandomGraphGenerator<>(10,23,seed);
        VertexFactory<String> vFactory = new VertexFactory<String>() {
            private int id = 0;
            @Override
            public String createVertex() {
                return "V"+(++id);
            }
        };

        generator.generateGraph(g, vFactory, null);

        return g;
    }


    private void contractEdges(){
        Set<DefaultEdge> allEdges=stringGraph.edgeSet();
        Random randomGenerator = new Random();
        int random= randomGenerator.nextInt(100)%allEdges.size();
        DefaultEdge[] myEdges= allEdges.toArray(new DefaultEdge[0]);
        String source=stringGraph.getEdgeSource(myEdges[random]) ;
        String target=stringGraph.getEdgeTarget(myEdges[random]);
        for(String vertex: stringGraph.vertexSet()){
            if(vertex!=source && vertex!=target && stringGraph.containsEdge(target,vertex)){
                stringGraph.removeEdge(target,vertex);
                stringGraph.addEdge(source,vertex);
            }
        }
        stringGraph.removeEdge(myEdges[random]);
        stringGraph.removeVertex(target);

    }
}
